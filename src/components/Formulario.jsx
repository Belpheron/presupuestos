import React, { useState } from "react";

import Error from "./Error";
import PropTypes from "prop-types";
import shortid from "shortid";

function Formulario({ guardarGasto, guardarCreargasto }) {
  const [nombre, guardarNombre] = useState("");
  const [cantidad, guardarCantidad] = useState(0);
  const [error, guardarError] = useState(false);

  const agregarGasto = (e) => {
    e.preventDefault();

    if (isNaN(cantidad) || cantidad < 1 || nombre === "") {
      guardarError(true);
      return;
    }

    guardarError(false);

    const gasto = {
      nombre,
      cantidad,
      id: shortid.generate(),
    };

    guardarGasto(gasto);
    guardarCreargasto(true);

    guardarNombre("");
    guardarCantidad(0);
  };

  return (
    <form onSubmit={agregarGasto}>
      <h2>Agrega tus gastos aquí</h2>

      {error ? <Error mensaje="Presupuesto incorrecto" /> : null}

      <div className="campo">
        <label>Nombre gasto</label>
        <input
          type="text"
          className="u-full-width"
          placeholder="Ej. transporte"
          value={nombre}
          onChange={(e) => guardarNombre(e.target.value)}
        />
      </div>
      <div className="campo">
        <label>Cantidad</label>
        <input
          type="number"
          className="u-full-width"
          placeholder="Ej. 300"
          value={cantidad}
          onChange={(e) => guardarCantidad(parseInt(e.target.value, 10))}
        />
      </div>
      <input
        type="submit"
        className="button-primary u-full-width"
        value="Agregar gasto"
      />
    </form>
  );
}

Formulario.propTypes = {
  guardarGasto: PropTypes.func.isRequired,
  guardarCreargasto: PropTypes.func.isRequired,
};

export default Formulario;
